import React, { Component } from "react";

class MovieTitle extends Component {
  render() {
    const { globalContent, outputType } = this.props;

    return (
      <div className="movie-title col-sm-12 col-md-8">
        <h1>Jurassic Park</h1>
      </div>
    );
  }
}

MovieTitle.label = "Movie Title";

export default MovieTitle;
