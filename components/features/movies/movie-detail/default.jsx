import React, { Component, Fragment } from "react";
import PropTypes from "prop-types";
import Consumer from "fusion:consumer";

@Consumer
class MovieDetail extends Component {
  constructor(props) {
    super(props);
    this.state = { isPlotShown: false };
    this.togglePlot = this.togglePlot.bind(this);
  }

  togglePlot() {
    const { isPlotShown } = this.state;
    // Create a common const `newPlotShown` for the next 2 lines to use
    const newPlotShown = !isPlotShown;
    this.setState({ isPlotShown: newPlotShown });

    // Dispatch an event called `moviePlotToggled` whose value is the new state of the plot's visibility
    this.dispatchEvent("moviePlotToggled", newPlotShown);
  }

  render() {
    const {
      moviePrefix = "Movie",
      showExtendedInfo = false,
    } = this.props.customFields;

    const { isPlotShown } = this.state;

    const plotButton = (
      <button onClick={this.togglePlot}>
        {isPlotShown ? "Hide Plot" : "Show Plot"}
      </button>
    );

    //const Plot = "Lorem ipsum";
    const { Actors, Director, Plot, Poster, Rated, Title, Writer, Year } =
      this.props.globalContent || {};

    return (
      <div className="movie-detail col-sm-12 col-md-8">
        {Title && (
          <h1>
            <span {...this.props.editableField("moviePrefix")}>
              {moviePrefix}:
            </span>{" "}
            {Title}
          </h1>
        )}
        {Director && (
          <p>
            <strong>Director:</strong> {Director}
          </p>
        )}
        {Actors && (
          <p>
            <strong>Actors:</strong> {Actors}
          </p>
        )}
        {Plot && (
          <p>
            <strong>Plot:</strong> {isPlotShown && Plot} {plotButton}
          </p>
        )}
        {showExtendedInfo && (
          <Fragment>
            {Rated && (
              <p>
                <strong>Rated:</strong> {Rated}
              </p>
            )}
            {Writer && (
              <p>
                <strong>Writer:</strong> {Writer}
              </p>
            )}
            {Year && (
              <p>
                <strong>Year:</strong> {Year}
              </p>
            )}
          </Fragment>
        )}
        {Poster && Title && <img src={Poster} alt={`Poster for ${Title}`} />}
      </div>
    );
  }
}

MovieDetail.label = "Movie Detail";

MovieDetail.propTypes = {
  customFields: PropTypes.shape({
    moviePrefix: PropTypes.string,
    showExtendedInfo: PropTypes.bool,
  }),
};

export default MovieDetail;
