import PropTypes from "prop-types";
import Consumer from "fusion:consumer";
import React, { Fragment, Component } from "react";
import "./style.scss";

@Consumer
class MovieList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      movies: {
        pages: [],
      },
      page: 0,
      showList: true,
    };
    this.fetch = this.fetch.bind(this);
  }

  componentDidMount() {
    this.fetch();
  }

  fetch() {
    const {
      contentService,
      contentConfigValues,
    } = this.props.customFields.movieListConfig;
    const { page } = this.state;

    const msgHandler = (plotShown) => {
      this.setState({ showList: !plotShown });
    };

    // Increment the page at each call
    this.state.page += 1;

    this.fetchContent({
      movies: {
        source: contentService,
        query: Object.assign(contentConfigValues, { page: page + 1 }),
        filter: "{ totalResults Search { Title Year Poster } }",
        transform: (data) => {
          // Check if data is being returned
          if (data && data.Search) {
            // Add the results to the paginated list of movies
            this.state.movies.pages[page] = data.Search;
            return this.state.movies;
          }

          // Otherwise just keep the current list of movies
          else {
            return this.state.movies;
          }
        },
      },
    });

    this.addEventListener("moviePlotToggled", msgHandler);
  }

  render() {
    if (typeof window === "undefined") return null;
    const { hideOnMobile } = this.props.displayProperties || {};
    if (hideOnMobile) return null;
    const movies = []
      .concat(...this.state.movies.pages)
      .filter((movie) => movie);
    const { showList } = this.state;

    return showList ? (
      <Fragment>
        <h2>Movies</h2>
        <div>
          {movies &&
            movies.map((movie, idx) => (
              <div key={`movie-${idx}`}>
                <h4>{movie.Title}</h4>
                <p>
                  <strong>Year:</strong> {movie.Year}
                </p>
                <img src={movie.Poster} className="image-sm" />
              </div>
            ))}
          <button onClick={this.fetch}>More</button>
        </div>
      </Fragment>
    ) : null;
  }
}

MovieList.propTypes = {
  customFields: PropTypes.shape({
    // We're using the Fusion-specific PropType `contentConfig` and passing it the name(s) of the GraphQL schemas this component will work with
    movieListConfig: PropTypes.contentConfig("movies").tag({
      group: "Configure Content",
    }),
  }),
};

export default MovieList;
