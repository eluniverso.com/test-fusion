import React, { Component } from "react";
import PropTypes from "prop-types";

class Message extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { message = "" } = this.props.customFields;

    return (
      <div className="movie-detail col-sm-12 col-md-8">
        <p>{message}</p>
      </div>
    );
  }
}

Message.label = "Message";

Message.propTypes = {
  customFields: PropTypes.shape({
    message: PropTypes.string,
  }),
};

export default Message;
