import React, { Component } from "react";
import Consumer from "fusion:consumer";
import ArticleBody from "@arc-core-components/feature_article-body"

@Consumer
class StoryDetail extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { headlines, content_elements } = this.props.globalContent || {};

    return (
      <div className="story-detail col-sm-12 col-md-8">
        <h1>{headlines.basic}</h1>
        <ArticleBody data={content_elements} />
      </div>
    );
  }
}

StoryDetail.label = "Story Detail";

export default StoryDetail;
