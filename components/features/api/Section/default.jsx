import React, { Component } from "react";
import PropTypes from "prop-types";
import Consumer from "fusion:consumer";

const WEBSITE_ID = "el-universo";

@Consumer
class Section extends Component {
  constructor(props) {
    super(props);
    const { customFields: { section, size = 3 } = {} } = props;

    this.fetchContent({
      section: {
        source: "section",
        query: {
          website: WEBSITE_ID,
          id: section,
        },
      },
      result: {
        source: "stories-by-section",
        query: {
          website: WEBSITE_ID,
          section,
          size,
        },
      },
    });
  }

  render() {
    const { section, result } = this.state || {};

    if (!section || !result) {
      return null;
    }

    const stories = result.content_elements || [];

    return (
      <div>
        <h3>
          Section {section.name} ({section._id})
        </h3>
        <ol>
          {stories.map((story) => (
            <li key={story._id}>{story.headlines.basic}</li>
          ))}
        </ol>
      </div>
    );
  }
}

Section.propTypes = {
  customFields: PropTypes.shape({
    section: PropTypes.string.tag({
      label: "Section ID",
    }).isRequired,
    size: PropTypes.number.tag({
      label: "# of Stories",
      defaultValue: 3,
    }),
  }),
};

export default Section;
