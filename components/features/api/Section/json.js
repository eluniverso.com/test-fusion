import PropTypes from "prop-types";
import Consumer from "fusion:consumer";

const WEBSITE_ID = "el-universo";

@Consumer
class Section {
  constructor(props) {
    this.props = props;
    const { customFields: { section, size = 3 } = {} } = props;

    this.fetchContent({
      section: {
        source: "section",
        query: {
          website: WEBSITE_ID,
          id: section,
        },
      },
      result: {
        source: "stories-by-section",
        query: {
          website: WEBSITE_ID,
          section,
          size,
        },
      },
    });
  }

  render() {
    const { section, result } = this.state || {};

    if (!section || !result) {
      return null;
    }

    return {
      id: section._id,
      name: section.name,
      stories: result.content_elements,
    };
  }
}

Section.propTypes = {
  customFields: PropTypes.shape({
    section: PropTypes.string.tag({
      label: "Section ID",
    }).isRequired,
    size: PropTypes.number.tag({
      label: "# of Stories",
      defaultValue: 3,
    }),
  }),
};

export default Section;
