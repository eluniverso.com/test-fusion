const Api = ({ children }) => {
  // Only return the data from the first child (body)
  return Array.isArray(children) ? children[0] : null;
};

Api.sections = ["body"];

export default Api;
