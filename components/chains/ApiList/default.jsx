import React from "react";
import PropTypes from "prop-types";

const ApiList = ({ children }) => <div>{children}</div>;

ApiList.propTypes = {
  children: PropTypes.any,
};

export default ApiList;
