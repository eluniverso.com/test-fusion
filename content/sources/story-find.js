const pattern = (key) => {
  const { website_url, website, id } = key;

  if (website_url)
    return `/content/v4/?website=el-universo&website_url=${website_url}`;

  if (!website_url && id)
    return `/content/v4/?website=el-universo&_id=${id}&published=false`;
};

const resolve = (key) => pattern(key);

const params = {
  website_url: "text",
  website: "site",
  id: "text",
};

const source = {
  resolve,
  params,
};

export default source;
