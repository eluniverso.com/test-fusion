const schema = `

  type Headline{
      basic: String!
      mobile: String
      native: String
  }

  type ContentElement{
      _id: String!
      type: String
      content: String
  }

  type Taxonomy{
      tags: [Tag]
      sections: [Section]
      primary_section: Section
  }

  type Section{
      _id
      name
      path
  }

  type Tag{
      _id
      name
  }

  type Query {
    headlines: Headline!
    content_elements: [ContentElement]!
    type: String!
    subtype: String
    created_date: String
    last_updated_date: String
    canonical_url: String
    display_date: String
    publish_date: String
    website_url: String
    taxonomy: Taxonomy
  }
`;

export default schema;
